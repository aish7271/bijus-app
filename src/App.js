import React from 'react';
import './App.css';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = { userName: "", filter: "", repoList: [], filteredRepoList: [] };
  };

  handleFetchInputChange = (event) => {
    this.setState({ userName: event.target.value });
  };

  handleFilterInputChange = (event) => {
    this.setState({ filter: event.target.value });
  };

  fetchRepo = () => {
    let URL = "https://api.github.com/users/" + this.state.userName + "/repos";

    fetch(URL)
      .then(res => res.json())
      .then(
        (result) => {
          let apiResult = result;
          let repos = []
          for (let i = 0; i < apiResult.length; i++) {
            repos.push(apiResult[i].url);
          }
          this.setState({ repoList: repos });
        },
        (error) => {

        }
      )
  };


  filterRepo = () => {
    let list = this.state.repoList;
    let filter = this.state.filter;
    let filteredList = [];
    for (let i = 0; i < list.length; i++) {
      if (list[i].includes(filter)) {
        filteredList.push(list[i]);
      }
    }
    this.setState({ repoList: filteredList });

  };

  clearRepo = () => {
    this.setState({ repoList: [] });
  };

  render() {
    return (
      <div className="App">
        <div className="functionality">
          <div className="fetch-div">
            <input className="fetch-git-repo-inout" type='text' onChange={this.handleFetchInputChange} />
            <br />
            <button className="fetch-git-repo-button" onClick={this.fetchRepo}>Fetch repo</button>
          </div>
          <div className="filter-div">
            <input className="filter-git-repo-button" type='text' onChange={this.handleFilterInputChange} />
            <br />
            <button className="filter-git-repo-button" onClick={this.filterRepo}>Filter repo</button>
            <br />
          </div >
          <div className="clear-div">
            <button className="clear-git-repo-button" onClick={this.clearRepo}>Clear repo</button>
          </div>
        </div>
        <div className="repo-list-div">
          {this.state.repoList.map((repo, index) => (
            <p> {repo}</p>
          ))}
        </div>
      </div >
    );
  }
}

export default App;
